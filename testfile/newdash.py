import dash
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output
import pandas as pd
from pycaret.classification import predict_model, load_model

final_model = load_model('ok-project/my_model')

app = dash.Dash(__name__)

app.layout = html.Div([
    
    html.Div([
        html.Label('ENG_SCORE'),
        dcc.Input(id='eng_score', type='number', min=0),
    ]),
    html.Div([
        html.Label('GPA_SCHOOL'),
        dcc.Input(id='gpa_school', type='number', min=0),
    ]),
    html.Div([
        html.Label('เกรดปี1เทอม1'),
        dcc.Input(id='grade11', type='number', min=0),
    ]),
    html.Div([
        html.Label('เกรดปี1เทอม2'),
        dcc.Input(id='grade12', type='number', min=0),
    ]),
    html.Div([
        html.Label('เกรดปี1เทอม3'),
        dcc.Input(id='grade13', type='number', min=0),
    ]),
    html.Div([
        html.Label('เกรดปี2เทอม1'),
        dcc.Input(id='grade21', type='number', min=0),
    ]),
    html.Div([
        html.Label('เกรดปี2เทอม2'),
        dcc.Input(id='grade22', type='number', min=0),
    ]),
    html.Div([
        html.Label('เกรดปี2เทอม3'),
        dcc.Input(id='grade23', type='number', min=0),
    ]),
    html.Div([
        html.Label('เกรดปี3เทอม1'),
        dcc.Input(id='grade31', type='number', min=0),
    ]),
    html.Div([
        html.Label('เกรดปี3เทอม2'),
        dcc.Input(id='grade32', type='number', min=0),
    ]),
    html.Div([
        html.Label('เกรดปี3เทอม3'),
        dcc.Input(id='grade33', type='number', min=0),
    ]),
    html.Div([
        html.Label('เกรดปี4เทอม1'),
        dcc.Input(id='grade41', type='number', min=0),
    ]),
    html.Div([
        html.Label('เกรดปี4เทอม2'),
        dcc.Input(id='grade42', type='number', min=0),
    ]),
    html.Div([
        html.Label('เกรดปี4เทอม3'),
        dcc.Input(id='grade43', type='number', min=0),
    ]),
    html.Br(),
    html.Button('Predict', id='predict-button'),
    html.Div(id='prediction-output')
])

@app.callback(
    Output('prediction-output', 'children'),
    Input('predict-button', 'n_clicks'),
    Input('eng_score', 'value'),
    Input('gpa_school', 'value'),
    Input('grade11', 'value'),
    Input('grade12', 'value'),
    Input('grade13', 'value'),
    Input('grade21', 'value'),
    Input('grade22', 'value'),
    Input('grade23', 'value'),
    Input('grade31', 'value'),
    Input('grade32', 'value'),
    Input('grade33', 'value'),
    Input('grade41', 'value'),
    Input('grade42', 'value'),
    Input('grade43', 'value')
)
def predict(n_clicks, eng_score, gpa_school, grade11, grade12, grade13, grade21, grade22, grade23, grade31, grade32, grade33, grade41, grade42, grade43):
    if n_clicks is None:
        return ''
    else:
        input_data = {'ENG_SCORE': eng_score,
                      'GPA_SCHOOL': gpa_school,
                      'เกรดปี1เทอม1': grade11,
                      'เกรดปี1เทอม2': grade12,
                      'เกรดปี1เทอม3': grade13,
                      'เกรดปี2เทอม1': grade21,
                      'เกรดปี2เทอม2': grade22,
                      'เกรดปี2เทอม3': grade23,
                      'เกรดปี3เทอม1': grade31,
                      'เกรดปี3เทอม2': grade32,
                      'เกรดปี3เทอม3': grade33,
                      'เกรดปี4เทอม1': grade41,
                      'เกรดปี4เทอม2': grade42,
                      'เกรดปี4เทอม3': grade43}
        input_df = pd.DataFrame([input_data])
        prediction = predict_model(final_model, data=input_df)
        predicted_class = prediction['Label'][0]
        return f'The predicted class is {predicted_class}'

# Run the app
if __name__ == '__main__':
    app.run_server(debug=True)
