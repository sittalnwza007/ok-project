from dash import Dash, dcc, html, Input, Output
import dash_bootstrap_components as dbc
import plotly.graph_objects as go
import plotly.express as px
import geopandas as gpd
import plotly.io as pio
import pandas as pd
import dash

app = dash.Dash(__name__, routes_pathname_prefix='/sss/')

app = Dash(__name__, external_stylesheets=[dbc.themes.CYBORG])

df1 = pd.read_excel("data_dropout_59-64.xlsx")
df2 = df1.drop(columns=[
                       'ADMIT_TERM', 
                       'FAC_ID',
                       'STUDY_LEVEL_ID',
                       'STUDY_LEVEL_NAME',
                       'FAC_NAME_THAI',
                       'STUDY_TYPE_NAME',
                       'STUDY_PLAN_NAME',
                       'DEGREE_ID',
                       'COURSE_NAME',
                       'COURSE_TYPE_NAME',
                       'NATIONALITY',
                       'SEX_NAME_THAI',
                       'ENT_METHOD_DESC',
                       'STUD_BIRTH_DATE',
                       'COUNTRY_NAME_ENG',
                       'PREV_INSTITUTION_NAME',
                       'INSTITUTION_PROVINCE_ID',
                       'INSTITUTION_PROVINCE_NAME',
                       'IS_GRAD_IN_COURSE',
                       'CURR_REG',
                       'IN_PROVINCE',
                       'IN_PROVINCE_CAMPUS',
                       'FUND_NAME',
                       'FUND_NAME_CODE',
                       'DEFORMITY_NAME_code',
                       'DEFORMITY_NAME',
                       'PARENTS_MARRIED_NAME',
                       'PARENTS_MARRIED_NAME_CODE',
                       'CAMPUS_ID',
                       'BIRTH_COUNTRY',
                       'เกรดปี1เทอม1',
                       'เกรดปี1เทอม2',
                       'เกรดปี1เทอม3',
                       'เกรดปี2เทอม1',
                       'เกรดปี2เทอม2',
                       'เกรดปี2เทอม3',
                       'เกรดปี3เทอม1',
                       'เกรดปี3เทอม2',
                       'เกรดปี3เทอม3',
                       'เกรดปี4เทอม1',
                       'เกรดปี4เทอม2',
                       'เกรดปี4เทอม3'])

df2 = df2.dropna(how='all')

sum_std = pd.read_excel("sum_of_std.xlsx")
thailand = gpd.read_file("thaishape/tha_admbnda_adm1_rtsd_20220121.shp")
province = pd.read_excel("total_province.xlsx")

merged = thailand.merge(province, on="ADM1_EN")
#-------------------------------------------------------------------------1-------------------------------------------------------------------------------------------------------------------------------------------------

@app.callback(Output("DEPT_NAME_THAI", "figure"),Input("year-slider", "value"))
def DEPT_NAME_THAI(selected_year): #ผู้โดยสารรวม
    filtered_df1 = df2[df2.ADMIT_YEAR == selected_year]

    fig2 = px.bar(
        filtered_df1,
        # y ="ADMIT_YEAR",
        x = "MAJOR_NAME_THAI",
        color="MAJOR_NAME_THAI",
        color_discrete_sequence=px.colors.sequential.RdBu,
        template="plotly_dark"
 
    )
    return fig2
#-------------------------------------------------------------------------2-------------------------------------------------------------------------------------------------------------------------------------------------

@app.callback(Output("Total-std", "figure"),Input("year-slider", "value"))
def number_of_students (selected_year):
    filtered_df2 = df2[df2.ADMIT_YEAR == selected_year]
    fig3 = px.choropleth(merged, 
                    geojson=merged.geometry, 
                    locations=merged.index,
                    color="total",
                    hover_data=["ADM1_EN", "p", "q"],
                    color_continuous_scale="RdBu"
                    ,template="plotly_dark")

    fig3.update_geos(fitbounds="locations", visible=False)
    return fig3 
#-------------------------------------------------------------------------4-------------------------------------------------------------------------------------------------------------------------------------------------
@app.callback(Output("num-std", "figure"),Input("year-slider", "value"))
def DEPT_NAME_THAI(selected_year): #ผู้โดยสารรวม
    filtered_df3 = sum_std[sum_std.y == selected_year]
    fig4 = px.sunburst(filtered_df3, path=['DEPT_NAME_THAI', 'MAJOR_NAME_THAI'], values='total', color='MAJOR_NAME_THAI'
        , color_discrete_sequence=px.colors.sequential.RdBu,template="plotly_dark")
   
                
    
    return fig4
#-------------------------------------------------------------------------4-------------------------------------------------------------------------------------------------------------------------------------------------
@app.callback(Output("Eng-score", "figure"),
            Input('data-dropdown', 'value'),
             Input('year-slider', 'value'))

# pie-chart
def Eng_scoree(selected_year,data_dropdown):
    filtered_df5 = df2[df2.ADMIT_YEAR == selected_year]

    fig2 = px.scatter(filtered_df5, y='ENG_SCORE',color_discrete_sequence=px.colors.sequential.RdBu,template="plotly_dark")
    # fig2.update_traces(mode='markers+lines', hovertemplate=None, 
    #                 line_color='#A48EFF', marker_color='#2883FF',
    #                 marker=dict(size=10))
    # fig2.update_layout(
    #             transition_duration=500,
    #             xaxis_title='STD',
    #             yaxis_title='Score',
    #             font=dict(
    #                 family="Arial",
    #                 size=12,
    #                 color="white"
    #             ),
    #         )

    return fig2

def update_graph2(data_type, selected_year):
    fig = Eng_scoree(selected_year, data_type)
    return fig
#-------------------------------------------------------------------------5-------------------------------------------------------------------------------------------------------------------------------------------------






























# @app.callback(Output("STUDY_STATUS", "figure"),Input("year-slider", "value"))
# def DEPT_NAME_THAI(selected_year): #ผู้โดยสารรวม
#     filtered_df2 = df2[df2.ADMIT_YEAR == selected_year]

#     fig2 = px.scatter(filtered_df2, x="STUDY_STATUS", y="ADMIT_YEAR", color='STUDY_STATUS', type = 'groupby',

#         template="plotly_dark")
    # data = [dict(
    #     type = 'scatter',
    #     x = "STUDY_STATUS",
    #     y = 'ADMIT_YEAR',
    #     mode = 'markers',
    #     transforms = [dict(
    #         type = 'groupby',
    #         groups = "STUDY_STATUS",
    #         styles = [
    #             dict(target = 'D', value = dict(marker = dict(color = 'blue'))),
    #             dict(target = 'E', value = dict(marker = dict(color = 'red'))),
    #             dict(target = 'G', value = dict(marker = dict(color = 'black'))),
    #             dict(target = 'I', value = dict(marker = dict(color = 'yellow'))),
    #             dict(target = 'OK', value = dict(marker = dict(color = 'green'))),
    #             dict(target = 'R', value = dict(marker = dict(color = 'pink')))
    #         ]
    #     )]
    #     )]
    # return fig2
#-------------------------------------------------------------------------LAYOUT-------------------------------------------------------------------------------------------------------------------------------------------------

app.layout = html.Div(
    [

        dbc.Row(
            [
                html.H1(
                    "DATA FROM ENG",
                    style={
                        "textAlign": "center",'font-family':'Georgia','color':'white','shadow':'True',
                    },
                )
            ],style={'margin-top': '70px', 'align':'center'},
            className="row",
        ),
        dbc.Row(
            [
                dbc.Col(
                    [
                        dcc.Slider(
                            df2["ADMIT_YEAR"].min(),
                            df2["ADMIT_YEAR"].max(),
                            step=None,
                            value=df2["ADMIT_YEAR"].min(),
                            marks={str(year): str(year) for year in df2["ADMIT_YEAR"].unique()},
                            id="year-slider",
                        ),
                    ],style={
                        "textAlign": "center",'font-family':'Georgia','color':'gold',}
                    
                    # className="col-sm",
                ),
            ],style={"padding-top": "40px;","padding-left":"70px"},
            
        ),
        
        dbc.Row(
            [dbc.Col(
                dbc.Card(
                    [dbc.CardHeader('DEPT NAME', style={'textAlign' : 'center', 'color':'yellow','font-family':'Georgia',}),
                        dcc.Graph(id="DEPT_NAME_THAI"),
                        
                    ],style={'height': '500px', 'borderRadius': '0px',},
                    # className="col-6",
                ),width={'size': 5},
               ),
            dbc.Col(
                dbc.Card(
                    [dbc.CardHeader('NUMBER OF STUDENTS IN EACH PROVINCE ', style={'textAlign' : 'center', 'color':'yellow','font-family':'Georgia',}),
                            dcc.Graph(id ='Total-std'),
                            
                        ],style={'height': '600px', 'borderRadius': '0px',},
                        # className="col-6",
                    ),width={'size': 5},
                ),
                ]
            ),
        dbc.Row(
            [dbc.Col(
                dbc.Card([dbc.CardHeader('NUMBER OF STUDENTS ADMIT', style={'textAlign' : 'center', 'color':'yellow','font-family':'Georgia',}),
                        #    dcc.Dropdown(id='values',
                        #         options=['ภาควิชาวิศวกรรมเครื่องกล','ภาควิชาวิศวกรรมอุตสาหการ','ภาควิชาวิศวกรรมไฟฟ้า','ภาควิชาวิศวกรรมเหมืองแร่และวัสดุ','ภาควิชาวิศวกรรมโยธา',
                        #                  'ภาควิชาวิศวกรรมเคมี','ภาควิชาวิศวกรรมคอมพิวเตอร์'],
                        #         # options=[{'label': DEPT_NAME_THAI, 'value': DEPT_NAME_THAI} for DEPT_NAME_THAI in sum_std['DEPT_NAME_THAI'].unique()],
                        #         value=sum_std['DEPT_NAME_THAI'].unique()[0],
                        #         ),  
                          dcc.Graph(id ='num-std'),
                           
                ],style={'height': '600px', 'borderRadius': '0px',},
                ),width={'size': 5},
            ),

            dbc.Col(
                    dbc.Card(
                        [   # แสดงจำนวนผู้โดยสารรวม, เฉลี่ยน รายวัน, วันธรรมดา, วันหยุด - line-graph
                            dbc.CardHeader('ENG SCORE', 
                                           style={'textAlign' : 'center', 
                                                  'font-family': 'Georgia', 
                                                  'color': '#D2D5FF', 
                                                  'font-size' : '20px'}
                                            ),
                            dcc.Dropdown(
                                id='data-dropdown',
                               options=[{'label': MAJOR_NAME_THAI, 'value': MAJOR_NAME_THAI} for MAJOR_NAME_THAI in df2['MAJOR_NAME_THAI'].unique()],
                                value='MAJOR_NAME_THAI',
                                style={'background-color': '#111111', 
                                       'color': '#111111',
                                       'border-color': '#5A58FF'
                                       }
                            ),
                            dcc.Graph(id='Eng-score', style={'border-radius': '10px', 
                                                              'overflow': 'hidden'}
                                                            ),
                        ],
                        # style={'height': '400px', 
                        #        'borderRadius': '10px', 
                        #        'background-color': '#111111', 
                        #        'border-color': '#5855FF', 
                        #        'border-width': '1px', 
                        #        'border-style': 'solid'
                        #        },
                    ),
                    width={'size': 6}
            )
            ]
    ),



    ],style={'margin-top': '70px', 'align':'center'}
)
  

if __name__ == "__main__":
    app.run_server(debug=True)