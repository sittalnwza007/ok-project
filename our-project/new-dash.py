from dash import Dash, dcc, html, Input, Output
import dash_bootstrap_components as dbc
from dash import html
from dash.dependencies import Input, Output
import plotly.graph_objects as go
import plotly.express as px
import plotly.figure_factory as ff
import plotly.io as pio
import pandas as pd
import dash
import geopandas as gpd
from pycaret.classification import predict_model, load_model

final_model = load_model('our-project/model_final.pkl')

app = dash.Dash(__name__, routes_pathname_prefix='/sss/')

app = Dash(__name__, external_stylesheets=[dbc.themes.JOURNAL])

df1 = pd.read_excel('our-project/data_for_dash_th.xlsx')
df1 = df1.dropna(how='all')

sum_std = pd.read_excel('Additional-data/sum_of_std.xlsx')
thailand = gpd.read_file('thaishape/tha_admbnda_adm1_rtsd_20220121.shp')
province = pd.read_excel('Additional-data/total_province.xlsx')
merged = thailand.merge(province, on='ADM1_EN')

my_colors = ['#8F2700', '#CA3700', '#FF4500', 
             '#FF6B34', '#FFA100', '#FFB434', 
             '#6DD4FF', '#00B3FF', '#367CFF',
             '#0052EA', '#3F3CFF', '#6D36FF',
             '#4200F0', '#0400F4', '#2F00A9']

my_colors2 = ['#f9d4b4'
              '#dbbc8c',
              '#b49c74',
              '#9c7d5c',
              '#a47454']

# Graph 1 ----------------------------------------------------------------------------------------------------------------------
@app.callback(Output('DEPT_NAME_THAI', 'figure'),
              Input('year-slider', 'value'))

def DEPT_NAME_THAI(selected_year):
    filtered_df = df1[df1.ADMIT_YEAR == selected_year]

    major_counts = filtered_df['MAJOR_NAME_THAI'].value_counts()

    fig1 = go.Figure(
        data=[
            go.Bar(
                x=major_counts.index, 
                y=major_counts.values,
                text=major_counts.values,
                textposition='auto',
                marker_color=my_colors
            )
        ]
    )

    fig1.update_layout(
        transition_duration=500,
        height=450,
        margin=dict(t=10, l=0, r=0, b=0),
        paper_bgcolor='rgba(0,0,0,0)'
    )

    fig1.update_xaxes(title_text='Major')
    fig1.update_yaxes(title_text='Number of Students')


    return fig1


# Graph 2 ----------------------------------------------------------------------------------------------------------------------
@app.callback(Output('Total-std', 'figure'),
              Input('year-slider', 'value'))

def number_of_students (selected_year):

    fig2 = px.choropleth(merged, 
                    geojson=merged.geometry, 
                    locations=merged.index,
                    color='total',
                    hover_data=['ADM1_EN', 'p', 'q'],
                    color_continuous_scale='PuOr',
                    template='seaborn'
                    )

    fig2.update_geos(
        fitbounds='locations', 
        visible=False
                    )
    
    fig2.update_layout(margin={'r':0,'t':0,'l':0,'b':0})
    return fig2 

# Graph 3 ----------------------------------------------------------------------------------------------------------------------
@app.callback(Output('num-std', 'figure'),
              Input('year-slider', 'value'))

def DEPT_NAME_THAI(selected_year):

    filtered_df = sum_std[sum_std.y == selected_year]

    fig3 = px.sunburst(filtered_df, path=['DEPT_NAME_THAI', 
                                          'MAJOR_NAME_THAI'], 
                                     values='total', 
                                     color='MAJOR_NAME_THAI', 
                                     color_discrete_sequence=my_colors,

                                     template='seaborn')
    fig3.update_layout(height=350, margin=dict(t=10, l=0, r=0, b=0), 
                   plot_bgcolor='rgba(0,0,0,0)', 
                   paper_bgcolor='rgba(0,0,0,0)',
                   font=dict(color='white'), 
                   sunburstcolorway=px.colors.qualitative.Pastel)
    fig3.update_traces(hovertemplate='<b>%{label} </b> <br> %{value} students')

   
    return fig3

# Graph 4 ----------------------------------------------------------------------------------------------------------------------

colors = ['#966BFF']

@app.callback(
    Output('eng-score', 'figure'),
    Input('year-slider', 'value'),
    Input('major-engscore', 'value'))

def eng_score(selected_year, major):
    filtered_df = df1[df1.ADMIT_YEAR == selected_year]

    if filtered_df.loc[filtered_df['MAJOR_NAME_THAI'] == major, 'ENG_SCORE'].empty:
        return {}
    else:
        hist_data = [filtered_df.loc[filtered_df['MAJOR_NAME_THAI'] == major, 'ENG_SCORE'].tolist()]
        group_labels = [major]

        fig4 = ff.create_distplot(hist_data, group_labels, 
                                  colors = my_colors,
                                  show_rug=False)

        fig4.update_layout(transition_duration=500,
                           height=400, margin=dict(t=5, l=0, r=0, b=0), 
                           paper_bgcolor='rgba(0,0,0,0)',
                           showlegend=False
                           )
        return fig4
    
# model ----------------------------------------------------------------------------------------------------------------------
@app.callback(
    Output('prediction-output', 'children'),
    Input('predict-button', 'n_clicks'),
    Input('eng_score', 'value'),
    Input('gpa_school', 'value'),
    Input('grade11', 'value'),
    Input('grade12', 'value'),
    Input('grade13', 'value'),
    Input('grade21', 'value'),
    Input('grade22', 'value'),
    Input('grade23', 'value'),
    Input('grade31', 'value'),
    Input('grade32', 'value'),
    Input('grade33', 'value'),
    Input('grade41', 'value'),
    Input('grade42', 'value'),
    Input('grade43', 'value')
)
def predict(n_clicks, eng_score, gpa_school, grade11, grade12, grade21, grade22,  grade31, grade32, grade41, grade42, grade33,grade23, grade13 ,grade43):
    if n_clicks is None:
        return ''
    else:
        input_data = {  'ENG_SCORE': eng_score,
                        'GPA_SCHOOL': gpa_school,
                        'เกรดปี1เทอม1': grade11,
                        'เกรดปี1เทอม2': grade12,
                        'เกรดปี1เทอม3': grade13,
                        'เกรดปี2เทอม1': grade21,
                        'เกรดปี2เทอม2': grade22,
                        'เกรดปี2เทอม3': grade23,
                        'เกรดปี3เทอม1': grade31,
                        'เกรดปี3เทอม2': grade32,
                        'เกรดปี3เทอม3': grade33,
                        'เกรดปี4เทอม1': grade41,
                        'เกรดปี4เทอม2': grade42,
                        'เกรดปี4เทอม3': grade43}
        input_df = pd.DataFrame([input_data])
        prediction = predict_model(final_model, data=input_df)
        predicted_class = prediction['prediction_label'][0]
        return predicted_class   

# Layout ----------------------------------------------------------------------------------------------------------------------
app.layout = html.Div(
    [   
        dbc.Container(
        [
        dbc.Row([
            dbc.Col([
                html.H2('MODEL-DATA', 
                        style={ 'color' : '#3a3420',
                                'textAlign': 'center', 
                                'margin-top': '10px',
                        }
                    )
                ],
            )
        ]),
        dbc.Row([
            dbc.Col(
                dcc.Slider(
                            df1['ADMIT_YEAR'].min(),
                            df1['ADMIT_YEAR'].max(),
                            step=None,
                            value=df1['ADMIT_YEAR'].min(),
                            marks={str(year): str(year) for year in df1['ADMIT_YEAR'].unique()},
                            id='year-slider'
                        ),
                        width={'size': 8, 'offset': 1},
                        style={'margin-top': '10px'}
            )
        ],
        justify='center'
        ),
        dbc.Row([
            dbc.Col(
                [   
                    dbc.Card(
                        [   
                            dbc.CardHeader('Number of Students in Each Department', 
                                           style={'textAlign' : 'center'}),
                            dcc.Graph(id='DEPT_NAME_THAI', 
                                      style={'height': '450px',
                                             'margin-left' : '20px',
                                             'margin-right' : '20px'})
                        ],
                        style={'borderRadius': '20px',
                               'box-shadow': '3px 3px 5px rgba(0, 0, 0, 0.2)',
                               'border-color': '#000000', 
                               'border-width': '1px', 
                               'border-style': 'solid',
                               'margin': '0 auto',
                               'margin-bottom' : '15px'}
                    ),
                    dbc.Card(
                        [
                            dbc.CardHeader('English Score', style={'textAlign' : 'center'}),
                            dcc.Dropdown(
                                id='major-engscore',
                                options=[{'label' : major, 'value' : major} for major in df1['MAJOR_NAME_THAI'].unique() 
                                         if major not in ['วิศวกรรมปัญญาประดิษฐ์', 'วิศวกรรมเหมืองแร่และวัสดุ', 'วิศวกรรมและการจัดการนวัตกรรม']],
                                value='วิศวกรรมเครื่องกล',
                                style={'background-color': 'white', 
                                    'border-color': '#E3E2E2', 
                                    'border-width': '1px', 
                                    'border-style': 'solid',
                                    }
                            ),
                            dcc.Graph(id='eng-score', style={'height': '425px',
                                                            'margin-left' : '20px',
                                                            'margin-right' : '20px',
                                                            'margin-bottom' : '5px'})
                        ],
                        style={'borderRadius': '20px',
                            'box-shadow': '3px 3px 5px rgba(0, 0, 0, 0.2)',
                            'border-color': '#000000', 
                            'border-width': '1px', 
                            'border-style': 'solid',
                            'margin': '0 auto',
                            'margin-bottom' : '15px'}
                    ),
                    dbc.Card(
                        [
                            dbc.CardHeader('MY MODEL', 
                                           style={'textAlign' : 'center'}),
                            dbc.Row(
                               [
                                    dbc.Col( #y1s1
                                        [
                                            dbc.Card(
                                                [
                                                    dbc.CardHeader('เกรดปี1เทอม1', 
                                                    
                                                    style={'textAlign' : 'center', 
                                                           'backgroundImage': 'linear-gradient(45deg, #C6A477, #ECD59F)',
                                                           'borderRadius': '20px 20px 0px 0px'}),
                                                           
                                                    dcc.Input(id='grade11', type='number', min=0,
                                                              style={'border-radius': '10px',
                                                                     'width' : '100px',
                                                                     'position' : 'absolute',
                                                                     'top' : '47px',
                                                                     'left' : '75px',
                                                                     'text-align' : 'center'})
                                                ],
                                                style={'borderRadius': '20px',
                                                        'box-shadow': '3px 3px 5px rgba(0, 0, 0, 0.2)',
                                                        'border-color': '#000000', 
                                                        'border-width': '1px', 
                                                        'border-style': 'solid',
                                                        'margin': '0 auto',
                                                        'magin-top' : '10px',
                                                        'margin-bottom' : '10px',
                                                        'height' : '85px',
                                                        'width' : '250px',
                                                        'backgroundColor': 'rgba(255, 255, 255, 0.5)'}
                                            )
                                        ]
                                    ),
                                    dbc.Col( #y1s2
                                        [
                                            dbc.Card(
                                                [
                                                    dbc.CardHeader('เกรดปี1เทอม2', 
                                                    
                                                    style={'textAlign' : 'center', 
                                                           'backgroundImage': 'linear-gradient(45deg, #C6A477, #ECD59F)',
                                                           'borderRadius': '20px 20px 0px 0px'}),
                                                           
                                                    dcc.Input(id='grade12', type='number', min=0,
                                                              style={'border-radius': '10px',
                                                                     'width' : '100px',
                                                                     'position' : 'absolute',
                                                                     'top' : '47px',
                                                                     'left' : '75px',
                                                                     'text-align' : 'center'})
                                                ],
                                                style={'borderRadius': '20px',
                                                        'box-shadow': '3px 3px 5px rgba(0, 0, 0, 0.2)',
                                                        'border-color': '#000000', 
                                                        'border-width': '1px', 
                                                        'border-style': 'solid',
                                                        'margin': '0 auto',
                                                        'magin-top' : '10px',
                                                        'margin-bottom' : '10px',
                                                        'height' : '85px',
                                                        'width' : '250px',
                                                        'backgroundColor': 'rgba(255, 255, 255, 0.5)'}
                                            )
                                        ]
                                    ),
                                    dbc.Col( #y1s3
                                        [
                                            dbc.Card(
                                                [
                                                    dbc.CardHeader('เกรดปี1เทอม3', 
                                                    
                                                    style={'textAlign' : 'center', 
                                                           'backgroundImage': 'linear-gradient(45deg, #C6A477, #ECD59F)',
                                                           'borderRadius': '20px 20px 0px 0px'}),
                                                           
                                                    dcc.Input(id='grade13', type='number', min=0,
                                                              style={'border-radius': '10px',
                                                                     'width' : '100px',
                                                                     'position' : 'absolute',
                                                                     'top' : '47px',
                                                                     'left' : '75px',
                                                                     'text-align' : 'center'})
                                                ],
                                                style={'borderRadius': '20px',
                                                        'box-shadow': '3px 3px 5px rgba(0, 0, 0, 0.2)',
                                                        'border-color': '#000000', 
                                                        'border-width': '1px', 
                                                        'border-style': 'solid',
                                                        'margin': '0 auto',
                                                        'magin-top' : '10px',
                                                        'margin-bottom' : '10px',
                                                        'height' : '85px',
                                                        'width' : '250px',
                                                        'backgroundColor': 'rgba(255, 255, 255, 0.5)'}
                                            )
                                        ]
                                    )
                               ],
                                justify='center',
                                style={'margin-top': '10px'} 
                            ),
                            dbc.Row(
                               [
                                    dbc.Col( #y2s1
                                        [
                                            dbc.Card(
                                                [
                                                    dbc.CardHeader('เกรดปี2เทอม1', 
                                                    
                                                    style={'textAlign' : 'center',
                                                           'backgroundImage': 'linear-gradient(45deg, #ECD59F, #D3E7EE)', 
                                                           'borderRadius': '20px 20px 0px 0px'}),

                                                    dcc.Input(id='grade21', type='number', min=0,
                                                              style={'border-radius': '10px',
                                                                     'width' : '100px',
                                                                     'position' : 'absolute',
                                                                     'top' : '47px',
                                                                     'left' : '75px',
                                                                     'text-align' : 'center'})
                                                ],
                                                style={'borderRadius': '20px',
                                                        'box-shadow': '3px 3px 5px rgba(0, 0, 0, 0.2)',
                                                        'border-color': '#000000', 
                                                        'border-width': '1px', 
                                                        'border-style': 'solid',
                                                        'margin': '0 auto',
                                                        'magin-top' : '10px',
                                                        'margin-bottom' : '10px',
                                                        'height' : '85px',
                                                        'width' : '250px',
                                                        'backgroundColor': 'rgba(255, 255, 255, 0.5)'}
                                            )
                                        ]
                                    ),
                                    dbc.Col( #y2s2
                                        [
                                            dbc.Card(
                                                [
                                                    dbc.CardHeader('เกรดปี2เทอม2', 
                                                    
                                                    style={'textAlign' : 'center',
                                                           'backgroundImage': 'linear-gradient(45deg, #ECD59F, #D3E7EE)', 
                                                           'borderRadius': '20px 20px 0px 0px'}),

                                                    dcc.Input(id='grade22', type='number', min=0,
                                                              style={'border-radius': '10px',
                                                                     'width' : '100px',
                                                                     'position' : 'absolute',
                                                                     'top' : '47px',
                                                                     'left' : '75px',
                                                                     'text-align' : 'center'})
                                                ],
                                                style={'borderRadius': '20px',
                                                        'box-shadow': '3px 3px 5px rgba(0, 0, 0, 0.2)',
                                                        'border-color': '#000000', 
                                                        'border-width': '1px', 
                                                        'border-style': 'solid',
                                                        'margin': '0 auto',
                                                        'magin-top' : '10px',
                                                        'margin-bottom' : '10px',
                                                        'height' : '85px',
                                                        'width' : '250px',
                                                        'backgroundColor': 'rgba(255, 255, 255, 0.5)'}
                                            )
                                        ]
                                    ),
                                    dbc.Col( #y2s3
                                        [
                                            dbc.Card(
                                                [
                                                    dbc.CardHeader('เกรดปี2เทอม3', 
                                                    
                                                    style={'textAlign' : 'center',
                                                           'backgroundImage': 'linear-gradient(45deg, #ECD59F, #D3E7EE)', 
                                                           'borderRadius': '20px 20px 0px 0px'}),

                                                    dcc.Input(id='grade23', type='number', min=0,
                                                              style={'border-radius': '10px',
                                                                     'width' : '100px',
                                                                     'position' : 'absolute',
                                                                     'top' : '47px',
                                                                     'left' : '75px',
                                                                     'text-align' : 'center'})
                                                ],
                                                style={'borderRadius': '20px',
                                                        'box-shadow': '3px 3px 5px rgba(0, 0, 0, 0.2)',
                                                        'border-color': '#000000', 
                                                        'border-width': '1px', 
                                                        'border-style': 'solid',
                                                        'margin': '0 auto',
                                                        'magin-top' : '10px',
                                                        'margin-bottom' : '10px',
                                                        'height' : '85px',
                                                        'width' : '250px',
                                                        'backgroundColor': 'rgba(255, 255, 255, 0.5)'}
                                            )
                                        ]
                                    )
                               ] 
                            ),
                            dbc.Row(
                               [
                                    dbc.Col( #y3s1
                                        [
                                            dbc.Card(
                                                [
                                                    dbc.CardHeader('เกรดปี3เทอม1', 
                                                    
                                                    style={'textAlign' : 'center',
                                                           'backgroundImage': 'linear-gradient(45deg, #D3E7EE, #ABD1DC)', 
                                                           'borderRadius': '20px 20px 0px 0px'}),
                                                    
                                                    dcc.Input(id='grade31', type='number', min=0,
                                                              style={'border-radius': '10px',
                                                                     'width' : '100px',
                                                                     'position' : 'absolute',
                                                                     'top' : '47px',
                                                                     'left' : '75px',
                                                                     'text-align' : 'center'})
                                                ],
                                                style={'borderRadius': '20px',
                                                        'box-shadow': '3px 3px 5px rgba(0, 0, 0, 0.2)',
                                                        'border-color': '#000000', 
                                                        'border-width': '1px', 
                                                        'border-style': 'solid',
                                                        'margin': '0 auto',
                                                        'magin-top' : '10px',
                                                        'margin-bottom' : '10px',
                                                        'height' : '85px',
                                                        'width' : '250px',
                                                        'backgroundColor': 'rgba(255, 255, 255, 0.5)'}
                                            )
                                        ]
                                    ),
                                    dbc.Col( #y3s2
                                        [
                                            dbc.Card(
                                                [
                                                    dbc.CardHeader('เกรดปี3เทอม2', 
                                                    
                                                    style={'textAlign' : 'center',
                                                           'backgroundImage': 'linear-gradient(45deg, #D3E7EE, #ABD1DC)', 
                                                           'borderRadius': '20px 20px 0px 0px'}),
                                                    
                                                    dcc.Input(id='grade32', type='number', min=0,
                                                              style={'border-radius': '10px',
                                                                     'width' : '100px',
                                                                     'position' : 'absolute',
                                                                     'top' : '47px',
                                                                     'left' : '75px',
                                                                     'text-align' : 'center'})
                                                ],
                                                style={'borderRadius': '20px',
                                                        'box-shadow': '3px 3px 5px rgba(0, 0, 0, 0.2)',
                                                        'border-color': '#000000', 
                                                        'border-width': '1px', 
                                                        'border-style': 'solid',
                                                        'margin': '0 auto',
                                                        'magin-top' : '10px',
                                                        'margin-bottom' : '10px',
                                                        'height' : '85px',
                                                        'width' : '250px',
                                                        'backgroundColor': 'rgba(255, 255, 255, 0.5)'}
                                            )
                                        ]
                                    ),
                                    dbc.Col( #y3s3
                                        [
                                            dbc.Card(
                                                [
                                                    dbc.CardHeader('เกรดปี3เทอม3', 
                                                    
                                                    style={'textAlign' : 'center',
                                                           'backgroundImage': 'linear-gradient(45deg, #D3E7EE, #ABD1DC)', 
                                                           'borderRadius': '20px 20px 0px 0px'}),
                                                    
                                                    dcc.Input(id='grade33', type='number', min=0,
                                                              style={'border-radius': '10px',
                                                                     'width' : '100px',
                                                                     'position' : 'absolute',
                                                                     'top' : '47px',
                                                                     'left' : '75px',
                                                                     'text-align' : 'center'})
                                                ],
                                                style={'borderRadius': '20px',
                                                        'box-shadow': '3px 3px 5px rgba(0, 0, 0, 0.2)',
                                                        'border-color': '#000000', 
                                                        'border-width': '1px', 
                                                        'border-style': 'solid',
                                                        'margin': '0 auto',
                                                        'magin-top' : '10px',
                                                        'margin-bottom' : '10px',
                                                        'height' : '85px',
                                                        'width' : '250px',
                                                        'backgroundColor': 'rgba(255, 255, 255, 0.5)'}
                                            )
                                        ]
                                    )
                               ] 
                            ),
                            dbc.Row(
                               [
                                    dbc.Col( #y4s1
                                        [
                                            dbc.Card(
                                                [
                                                    dbc.CardHeader('เกรดปี4เทอม1', 
                                                    
                                                    style={'textAlign' : 'center',
                                                           'backgroundImage': 'linear-gradient(45deg, #ABD1DC, #7097A8)', 
                                                           'borderRadius': '20px 20px 0px 0px'}),

                                                    dcc.Input(id='grade41', type='number', min=0,
                                                              style={'border-radius': '10px',
                                                                     'width' : '100px',
                                                                     'position' : 'absolute',
                                                                     'top' : '47px',
                                                                     'left' : '75px',
                                                                     'text-align' : 'center'})
                                                ],
                                                style={'borderRadius': '20px',
                                                        'box-shadow': '3px 3px 5px rgba(0, 0, 0, 0.2)',
                                                        'border-color': '#000000', 
                                                        'border-width': '1px', 
                                                        'border-style': 'solid',
                                                        'margin': '0 auto',
                                                        'magin-top' : '10px',
                                                        'margin-bottom' : '10px',
                                                        'height' : '85px',
                                                        'width' : '250px',
                                                        'backgroundColor': 'rgba(255, 255, 255, 0.5)'}
                                            )
                                        ]
                                    ),
                                    dbc.Col( #y4s2
                                        [
                                            dbc.Card(
                                                [
                                                    dbc.CardHeader('เกรดปี4เทอม2', 
                                                    
                                                    style={'textAlign' : 'center',
                                                           'backgroundImage': 'linear-gradient(45deg, #ABD1DC, #7097A8)', 
                                                           'borderRadius': '20px 20px 0px 0px'}),

                                                    dcc.Input(id='grade42', type='number', min=0,
                                                              style={'border-radius': '10px',
                                                                     'width' : '100px',
                                                                     'position' : 'absolute',
                                                                     'top' : '47px',
                                                                     'left' : '75px',
                                                                     'text-align' : 'center'})
                                                ],
                                                style={'borderRadius': '20px',
                                                        'box-shadow': '3px 3px 5px rgba(0, 0, 0, 0.2)',
                                                        'border-color': '#000000', 
                                                        'border-width': '1px', 
                                                        'border-style': 'solid',
                                                        'margin': '0 auto',
                                                        'magin-top' : '10px',
                                                        'margin-bottom' : '10px',
                                                        'height' : '85px',
                                                        'width' : '250px',
                                                        'backgroundColor': 'rgba(255, 255, 255, 0.5)'}
                                            )
                                        ]
                                    ),
                                    dbc.Col( #y4s3
                                        [
                                            dbc.Card(
                                                [
                                                    dbc.CardHeader('เกรดปี4เทอม3', 
                                                    
                                                    style={'textAlign' : 'center',
                                                           'backgroundImage': 'linear-gradient(45deg, #ABD1DC, #7097A8)', 
                                                           'borderRadius': '20px 20px 0px 0px'}),

                                                    dcc.Input(id='grade43', type='number', min=0,
                                                              style={'border-radius': '10px',
                                                                     'width' : '100px',
                                                                     'position' : 'absolute',
                                                                     'top' : '47px',
                                                                     'left' : '75px',
                                                                     'text-align' : 'center'})
                                                ],
                                                style={'borderRadius': '20px',
                                                        'box-shadow': '3px 3px 5px rgba(0, 0, 0, 0.2)',
                                                        'border-color': '#000000', 
                                                        'border-width': '1px', 
                                                        'border-style': 'solid',
                                                        'margin': '0 auto',
                                                        'magin-top' : '10px',
                                                        'margin-bottom' : '10px',
                                                        'height' : '85px',
                                                        'width' : '250px',
                                                        'backgroundColor': 'rgba(255, 255, 255, 0.5)'}
                                            )
                                        ]
                                    )
                               ] 
                            ),
                            dbc.Row(
                               [
                                    dbc.Col( #GPA
                                        [
                                            dbc.Card(
                                                [
                                                    dbc.CardHeader('ENG_SCORE', 
                                                    
                                                    style={'textAlign' : 'center',
                                                           'backgroundImage': 'linear-gradient(45deg, #7097A8, #ECD59F)',
                                                           'borderRadius': '20px 20px 0px 0px'}),

                                                    dcc.Input(id='eng_score', type='number', min=0,
                                                              style={'border-radius': '10px',
                                                                     'width' : '100px',
                                                                     'position' : 'absolute',
                                                                     'top' : '47px',
                                                                     'left' : '75px',
                                                                     'text-align' : 'center'})
                                                ],
                                                style={'borderRadius': '20px',
                                                        'box-shadow': '3px 3px 5px rgba(0, 0, 0, 0.2)',
                                                        'border-color': '#000000', 
                                                        'border-width': '1px', 
                                                        'border-style': 'solid',
                                                        'margin': '0 auto',
                                                        'magin-top' : '10px',
                                                        'margin-bottom' : '10px',
                                                        'height' : '85px',
                                                        'width' : '250px',
                                                        'backgroundColor': 'rgba(255, 255, 255, 0.5)'}
                                            )
                                        ]
                                    ),
                                    dbc.Col( #Eng score
                                        [
                                            dbc.Card(
                                                [
                                                    dbc.CardHeader('GPA_SCHOOL', 
                                                    
                                                    style={'textAlign' : 'center',
                                                           'backgroundImage': 'linear-gradient(45deg, #7097A8, #ECD59F)',
                                                           'borderRadius': '20px 20px 0px 0px'}),

                                                    dcc.Input(id='gpa_school', type='number', min=0,
                                                              style={'border-radius': '10px',
                                                                     'width' : '100px',
                                                                     'position' : 'absolute',
                                                                     'top' : '47px',
                                                                     'left' : '75px',
                                                                     'text-align' : 'center'})
                                                ],
                                                style={'borderRadius': '20px',
                                                        'box-shadow': '3px 3px 5px rgba(0, 0, 0, 0.2)',
                                                        'border-color': '#000000', 
                                                        'border-width': '1px', 
                                                        'border-style': 'solid',
                                                        'margin': '0 auto',
                                                        'magin-top' : '10px',
                                                        'margin-bottom' : '10px',
                                                        'height' : '85px',
                                                        'width' : '250px',
                                                        'backgroundColor': 'rgba(255, 255, 255, 0.5)'}
                                            )
                                        ]
                                    )
                               ] 
                            ),
                            dbc.Row(
                                [
                                    html.Br(),
                                    html.Button('Predict', id='predict-button'),
                                ],
                                style={'borderRadius': '20px',
                                        'border-color': '#ffffe0', 
                                        'margin': '0 auto'}

                            )
                        ],
                        style={'borderRadius': '20px',
                               'box-shadow': '3px 3px 5px rgba(0, 0, 0, 0.2)',
                               'border-color': '#000000', 
                               'border-width': '1px', 
                               'border-style': 'solid',
                               'margin': '0 auto',
                               'margin-bottom' : '15px',
                               'height' : '570px',
                               'backgroundImage': 'linear-gradient(45deg, #FFE5B7, #A0C1FF)'
                               }

                    ) 
                ],
                width={'size': 8}
            ),
            dbc.Col(
                [
                    dbc.Card(
                        [
                            dbc.CardHeader('Study Status', 
                                        style={'textAlign' : 'center'}),
                            dbc.Row(
                                [
                                    dbc.Col(
                                        [
                                            dbc.Card(
                                                [   
                                                    html.H3('I', style={'textAlign': 'center', 'margin-top': '10px'}),
                                                    html.Hr(style={'margin': '10px'}),
                                                    html.H4('3', style={'textAlign': 'center'}),
                                                    html.P('people', style={'textAlign': 'center'})
                                                ],
                                                style={'borderRadius': '20px',
                                                    'box-shadow': '3px 3px 5px rgba(0, 0, 0, 0.2)',
                                                    'border-color': '#000000', 
                                                    'border-width': '1px', 
                                                    'border-style': 'solid',
                                                    'margin': '0 auto',
                                                    'backgroundColor' : '#FFE5B7'}
                                            )
                                        ],
                                        width={'size': 3, 'offset': 1/2}
                                        ),
                                        dbc.Col(
                                            [
                                                dbc.Card(
                                                    [
                                                        html.H3('G', style={'textAlign': 'center',
                                                                            'margin-top': '10px'}),
                                                        html.Hr(style={'margin': '10px'}),
                                                        html.H4('1483', style={'textAlign': 'center'}),
                                                        html.P('people', style={'textAlign': 'center'})
                                                    ],
                                                    style={'borderRadius': '20px',
                                                        'box-shadow': '3px 3px 5px rgba(0, 0, 0, 0.2)',
                                                        'border-color': '#000000', 
                                                        'border-width': '1px', 
                                                        'border-style': 'solid',
                                                        'margin': '0 auto',
                                                        'backgroundColor' : '#FFCF7C'}
                                                )
                                            ],
                                            width={'size': 3, 'offset': 1/2}
                                        ),
                                        dbc.Col(
                                            [
                                                dbc.Card(
                                                    [
                                                        html.H3('R', style={'textAlign': 'center',
                                                                            'margin-top': '10px'}),
                                                        html.Hr(style={'margin': '10px'}),
                                                        html.H4('214', style={'textAlign': 'center'}),
                                                        html.P('people', style={'textAlign': 'center'})
                                                    ],
                                                    style={'borderRadius': '20px',
                                                        'box-shadow': '3px 3px 5px rgba(0, 0, 0, 0.2)',
                                                        'border-color': '#000000', 
                                                        'border-width': '1px', 
                                                        'border-style': 'solid',
                                                        'margin': '0 auto',
                                                        'backgroundColor' : '#FF966F'}
                                                )
                                            ],
                                            width={'size': 3, 'offset': 1/2}
                                        )
                                    ],
                                    justify='center',
                                    style={'margin-top': '10px'} 
                                ),
                                dbc.Row(
                                    [
                                        dbc.Col(
                                            [
                                                dbc.Card(
                                                    [   
                                                        html.H3('OK', style={'textAlign': 'center',
                                                                            'margin-top': '10px'}),
                                                        html.Hr(style={'margin': '10px'}),
                                                        html.H4('2139', style={'textAlign': 'center'}),
                                                        html.P('people', style={'textAlign': 'center'})
                                                    ],
                                                    style={'borderRadius': '20px',
                                                        'box-shadow': '3px 3px 5px rgba(0, 0, 0, 0.2)',
                                                        'border-color': '#000000', 
                                                        'border-width': '1px', 
                                                        'border-style': 'solid',
                                                        'margin': '0 auto',
                                                        'backgroundColor' : '#ABE6FF'}
                                                )
                                            ],
                                            width={'size': 3, 'offset': 1/2}
                                        ),
                                        dbc.Col(
                                            [
                                                dbc.Card(
                                                    [
                                                        html.H3('E', style={'textAlign': 'center',
                                                                            'margin-top': '10px'}),
                                                        html.Hr(style={'margin': '10px'}),
                                                        html.H4('270', style={'textAlign': 'center'}),
                                                        html.P('people', style={'textAlign': 'center'})
                                                    ],
                                                    style={'borderRadius': '20px',
                                                        'box-shadow': '3px 3px 5px rgba(0, 0, 0, 0.2)',
                                                        'border-color': '#000000', 
                                                        'border-width': '1px', 
                                                        'border-style': 'solid',
                                                        'margin': '0 auto',
                                                        'backgroundColor' : '#A0C1FF'}
                                                )
                                            ],
                                            width={'size': 3, 'offset': 1/2}
                                        ),
                                        dbc.Col(
                                            [
                                                dbc.Card(
                                                    [
                                                        html.H3('D', style={'textAlign': 'center',
                                                                            'margin-top': '10px'}),
                                                        html.Hr(style={'margin': '10px'}),
                                                        html.H4('11', style={'textAlign': 'center'}),
                                                        html.P('people', style={'textAlign': 'center'})
                                                    ],
                                                    style={'borderRadius': '20px',
                                                        'box-shadow': '3px 3px 5px rgba(0, 0, 0, 0.2)',
                                                        'border-color': '#000000', 
                                                        'border-width': '1px', 
                                                        'border-style': 'solid',
                                                        'margin': '0 auto',
                                                        'margin-bottoms': '10px',
                                                        'backgroundColor' : '#A9A8FF'}
                                                )
                                            ],
                                            width={'size': 3, 'offset': 1/2}
                                        )
                                    ],
                                    justify='center',
                                    style={'margin-top': '10px',
                                           'margin-bottom' : '10px'} 
                            )   
                        ],
                        style={'borderRadius': '20px',
                            'box-shadow': '3px 3px 5px rgba(0, 0, 0, 0.2)',
                            'border-color': '#000000', 
                            'border-width': '1px', 
                            'border-style': 'solid',
                            'margin': '0 auto',
                            'margin-bottom' : '15px',
                            'backgroundImage': 'linear-gradient(45deg, #FFE5B7, #ABE6FF)'}
                    ),
                    dbc.Card(
                        [   
                            dbc.CardHeader('Number of Students Admit', 
                                            style={'textAlign' : 'center'}),
                            dcc.Graph(id ='num-std',
                                        style={'height': '350px',
                                            'margin-left' : '20px',
                                            'margin-right' : '20px',
                                            'margin-bottom' : '10px'})
                        ],
                        style={'borderRadius': '20px',
                            'box-shadow': '3px 3px 5px rgba(0, 0, 0, 0.2)',
                            'border-color': '#000000', 
                            'border-width': '1px', 
                            'border-style': 'solid',
                            'margin': '0 auto',
                            'margin-bottom' : '15px'}
                    ),
                    dbc.Card(
                                [               
                                    dbc.CardHeader('Number of Students in each Province ', 
                                                style={'textAlign' : 'center'}),
                                    dcc.Graph(id ='Total-std',
                                            style={'height': '450px',
                                                    'margin-left' : '20px',
                                                    'margin-right' : '20px'})            
                                ],
                                style={'borderRadius': '20px',
                                    'box-shadow': '3px 3px 5px rgba(0, 0, 0, 0.2)',
                                    'border-color': '#000000', 
                                    'border-width': '1px', 
                                    'border-style': 'solid',
                                    'margin': '0 auto',
                                    'margin-bottom' : '15px'}
                    ),
                    dbc.Card(
                        [   
                            dbc.CardBody([
                                    html.H4("Prediction Output 📕", className="card-title"),
                                    html.Hr(style={'margin': '10px'}),
                                    html.H3(id='prediction-output', className='card-text',
                                            style={'text-color': '#ffffff',
                                                   'text-shadow': '2px 2px 4px #686868'
                                                   })
                                ])
                        ],
                        style={'borderRadius': '20px',
                                'box-shadow': '3px 3px 5px rgba(0, 0, 0, 0.2)',
                                'border-color': '#000000', 
                                'border-width': '1px', 
                                'border-style': 'solid',
                                'margin': '0 auto',
                                'backgroundImage': 'linear-gradient(45deg, #ABD1DC, #ECD59F)'}   
                    )
                ]
            )
        ],
        style={'margin-bottom': '10px'} 
        ),
                dbc.Row(
                    [   
                    dbc.Col(
                        [       
                        
                        ],
                        width={'size': 4}
                    ),
                    dbc.Col(
                                    [
                                        
                                    ],
                                                width={'size': 8}  
                                )   
                            ]
                        )
                    ]
                )                                                      
                                                                         
    ],
    style = {'background-image': 'url("https://cdn.pixabay.com/photo/2016/02/06/13/39/steampunk-1182929_1280.jpg")',
            'background-filter': 'blur(100px)',
            'background-size': 'cover',
            'background-repeat': 'no-repeat',
            'background-position': 'center center',}
)   
                                                
if __name__ == "__main__":
    app.run_server(debug=False)
