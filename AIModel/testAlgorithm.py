import pandas as pd

def calculate_status(grade, prev_grade, prev_status):
    if grade > 2:
        return "Reset"
    elif grade < 1.5:
        return "Retire"
    elif grade >= 2 and prev_grade == -1:
        return "Pass"
    elif grade >= 2 and prev_grade >= 2:
        return "Pass"
    elif grade >= 2 and 1.7 <= prev_grade < 2:
        return "Second Pro"
    elif grade >= 2 and 1.5 <= prev_grade < 1.7:
        return "First Pro"
    elif grade >= 1.9 and prev_status == "First Pro":
        return "Second Pro"
    elif grade >= 1.7 and prev_status == "Pass":
        return "First Pro"
    elif 1.25 <= grade < 2 and prev_status == "Retire":
        return "First Pro"
    elif grade >= 1 and grade < 2 and prev_status == "Pass":
        return "Critical"
    elif grade >= 1 and grade < 2 and prev_status == "First Pro":
        return "Critical"
    elif grade >= 1 and grade < 2 and prev_status == "Second Pro":
        return "Critical"
    elif grade < 1:
        return "Retire"
    else:
        return "unknown"
data = pd.read_excel('data_status_59-64.xlsx', usecols=[
    'STUDENT_ID',
    'เกรดปี1เทอม1', 'เกรดปี1เทอม2', 'เกรดปี1เทอม3',
    'เกรดปี2เทอม1', 'เกรดปี2เทอม2', 'เกรดปี2เทอม3',
    'เกรดปี3เทอม1', 'เกรดปี3เทอม2', 'เกรดปี3เทอม3',
    'เกรดปี4เทอม1', 'เกรดปี4เทอม2', 'เกรดปี4เทอม3',
])

data = data.set_index('STUDENT_ID')

statuses = {}

for student_id, row in data.iterrows():
    prev_grade = -1
    prev_status = ""
    num_retire = 0
    num_grades = 0
    for col, grade in row.iteritems():
        if pd.isna(grade):
            continue
        num_grades += 1
        if prev_grade != -1:
            status = calculate_status(grade, prev_grade, prev_status)
        else:
            status = calculate_status(grade, -1, "")
        if student_id not in statuses:
            statuses[student_id] = [status]
        else:
            statuses[student_id].append(status)
        if status == 'Retire':
            num_retire += 1
        prev_grade = grade
        prev_status = status
    if num_grades > 0:
        percent_retire = (num_retire / num_grades) * 100
        print(f"Student ID: {student_id}")
        print(f"Percent Retire: {percent_retire}%\n")


#คำนวณเปอร์เซ็นต์ของนักศึกษา Retire จากการเรียนของแต่ละคน
# นับจากจำนวนเทอมที่ถอนตัวเป็นส่วนของจำนวนเทอมที่เคยลงทะเบียนเรียนแล้วคูณด้วย 100%
# แสดงผลเป็นเปอร์เซ็นต์ของจำนวนเทอมที่ถอนตัว เช่น 
# ถ้านักศึกษาเคยลงทะเบียนเรียน 9 เทอมและถอนตัวใน 3 เทอม ก็จะได้เปอร์เซ็นต์ของการถอนตัวเป็น 33.33%